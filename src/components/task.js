import React, { useEffect, useState } from 'react';

import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import './style.css';
import loadingImg from '../image/loadingspinner.gif'




function Task() {

  
  const [isData, setIsData] = useState(false);
  const [chatListDatas, setchatListData] = useState([])
  const [chatLists, setChatList] = useState([]);

  const [startDate, setStartDate] = useState(new Date(2019, 2, 31, 10, 33, 30, 0));
 
  const [endDate, setEndDate] = useState(null);
  useEffect(() => {
    axios.get('http://localhost:8080/data').then((res) => {
      setchatListData(res.data);

    })
  }, []);

  const onChange = dates => {
    const [start, end] = dates;
    setStartDate(start);
    
    setEndDate(end);
    const stringStartdate = new Date(start).toISOString()
    const stringEnddate = new Date(end).toISOString()
    
    let matchData = [];
    matchData = chatListDatas.data.filter(function (e) {
      return e.date >= stringStartdate && e.date <= stringEnddate;
    });
    setChatList(matchData)
    setIsData(true)
    
  }

  return (

    <div className="container">
      
      <div>
        <DatePicker
          selected={startDate}
          onChange={onChange}
          startDate={startDate}
          endDate={endDate}
          selectsRange
          inline
        />
      </div>
      <div className="row mt-5">
        {chatListDatas.length == 0 ? <img src={loadingImg} alt="loading..."/>:
        isData && chatLists.length == 0 ? <h1> no chats fond in selected date range</h1> :
         chatLists.length > 0 ? chatLists.map((data) => (


            <div className="col-md-4" key={data.index}>
              <div className="card border-info mb-3">
               
                <div className="card-header headerText">{moment.utc(data.date).format('DD-MMM-YYYY hh:mm:ss A')}</div>
                <div className="card-body text-info cardText">
                <span className="card-text mr-1">WebSiteId:</span> <span>{data.websiteId}</span>
                  <br></br>
                  <span className="card-text mr-1">Chats:</span><span>{data.chats}</span>
                  <br></br>
                  <span className="card-text mr-1">Missed Chats:</span><span>{data.missedChats}</span>
                </div>
              </div>
            </div>

          )) : chatListDatas.data.length > 0 && chatListDatas.data.map((data) => (


            <div className="col-md-4" key={data.index}>
              <div className="card border-info mb-3">
              
                <div className="card-header headerText">{moment.utc(data.date).format('DD-MMM-YYYY hh:mm:ss A')}</div>
                <div className="card-body text-info cardText">
                  <span className="card-text mr-1">WebSiteId:</span> <span>{data.websiteId}</span>
                  <br></br>
                  <span className="card-text mr-1">Chats:</span> <span>{data.chats}</span>
                  <br></br>
                  <span className="card-text mr-1">Missed Chats:</span><span>{data.missedChats}</span>
                </div>
              </div>
            </div>

          ))};
 </div>
    </div>
  );
}

export default Task;